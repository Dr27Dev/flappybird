using System;
using System.Collections;
using UnityEngine;

namespace FlappyBird
{
    public class Pipe : MonoBehaviour
    {
        [SerializeField] private float speed;

        [SerializeField] private GameObject dayPipes;
        [SerializeField] private GameObject nightPipes;

        private void Start()
        {
            if (GameManager.Instance.IsDaytime)
            {
                dayPipes.SetActive(true);
                nightPipes.SetActive(false);
            }
            else
            {
                dayPipes.SetActive(false);
                nightPipes.SetActive(true);
            }
        }

        private void Update()
        {
            if (GameManager.Instance.isGameOver) return;
            transform.position += (Vector3.left * Time.deltaTime * speed);
        }
    }
}