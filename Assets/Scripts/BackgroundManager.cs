using System;
using System.Collections;
using System.Collections.Generic;
using FlappyBird;
using UnityEngine;

public class BackgroundManager : MonoBehaviour
{
    private SpriteRenderer spriteRenderer;
    [SerializeField] private Sprite daySprite;
    [SerializeField] private Sprite nightSprite;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = GameManager.Instance.IsDaytime ? daySprite : nightSprite;
    }
}
