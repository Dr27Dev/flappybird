using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FlappyBird
{
    public class PipeSpawner : MonoBehaviour
    {
        [SerializeField]
        private GameObject pipePrefab;
        [SerializeField]
        private Transform spawnPoint;

        [Space, SerializeField, Range(-1, 1)]
        private float minHeight;
        [SerializeField, Range(-1, 1)]
        private float maxHeight;

        [Space, SerializeField]
        private float timeToSpawnFirstPipe;
        [SerializeField]
        private float timeToSpawnPipe;

        private Queue<GameObject> pipePool = new Queue<GameObject>();
        [SerializeField] private int poolSize = 5;
        [SerializeField] private float pipeLifeTime = 5;

        private void Start()
        {
            StartCoroutine(SpawnPipes());
            FillPool();
        }

        private Vector3 GetSpawnPosition()
        {
            return new Vector3(spawnPoint.position.x, Random.Range(minHeight, maxHeight), spawnPoint.position.z);
        }

        private IEnumerator SpawnPipes()
        {
            yield return new WaitForSeconds(timeToSpawnFirstPipe);

            StartCoroutine(SetPipe());

            do
            {
                yield return new WaitForSeconds(timeToSpawnPipe);
                StartCoroutine(SetPipe());
            } while (true);
        }

        private void FillPool()
        {
            for (int i = 0; i < poolSize; i++)
            {
                GameObject pipe = Instantiate(pipePrefab);
                pipe.SetActive(false);
                pipePool.Enqueue(pipe);
            }
        }

        private GameObject GetPipeFromPool()
        {
            if (pipePool.Count <= 0)
            {
                GameObject pipe = Instantiate(pipePrefab);
                return pipe;
            }
            else
            {
                GameObject pipe = pipePool.Dequeue();
                pipe.SetActive(true);
                return pipe;
            }
        }

        private void ReturnPipeToPool(GameObject pipe)
        {
            pipe.SetActive(false);
            pipePool.Enqueue(pipe);
        }

        private IEnumerator SetPipe()
        {
            GameObject currentPipe = GetPipeFromPool();
            currentPipe.transform.position = GetSpawnPosition();
            yield return new WaitForSeconds(pipeLifeTime);
            ReturnPipeToPool(currentPipe);
        }


        public void Stop()
        {
            StopAllCoroutines();
        }
    }
}